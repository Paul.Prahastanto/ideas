(Teks dalam tanda kurung adalah contoh dan bisa dihapus untuk diganti dengan laporan Anda)

### Problem to solve

(Isikan permasalahan yang ingin diselesaikan dengan penambahan fitur)

### Intended users

(Pengguna dari fitur baru ini, apakah seluruh pegawai/ UPK/ Pimpinan, dsb)

### Further details

(Penjelasan lebih detail atas proposal fitur kaitannya dengan Benefitnya/ Tujuannya)

### Proposal

(Silakan di deskripsikan bagaimana fitur baru ini bekerja, sehingga memudahkan tim programmer untuk membuatnya)

### Documentation

(Jika Anda sudah membuat mock up atas fitur ini, silakan isikan dokumentasi atau mock up atas fitur tersebut)

### What does success look like, and how can we measure that?

(Tuliskan bagaimana kami mengukur keberhasilan penambahan fitur ini)

### Links / references

/label ~feature