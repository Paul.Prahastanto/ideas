(Harap dibaca!
Sebelum melaporkan Bug, pastikan Anda sudah mencari di issue queue bahwa bug ini belum pernah dilaporkan sebelumnya.
Silakan cek di:
- https://gitlab.com/hrd-djp/ideas/issues?label_name%5B%5D=bug

Dan pastikan belum ada bug yang sejenis.
Silakan isikan permasalahan Anda sesuai seperti contoh dalam tanda kurung dibawah. Tanda kurung dibawah dan seluruh teks di dalam tanda kurung ini boleh dihapus)

### Summary

(Tuliskan ringkasan permasalahan Anda disini)

### Steps to reproduce

(Tuliskan langkah-langkah yang Saudara lakukan sampai dengan bug terjadi)

### What is the current *bug* behavior?

(Apa dampak yang terjadi/ yang terlihat dari adanya bug ini)

### What is the expected *correct* behavior?

(Bagaimana seharusnya sistem memberikan respon apabila bug tidak ada)

### Relevant logs and/or screenshots

(Unggah screenshot atau foto bug ini)

### Possible fixes

(Jika Anda bisa memberikan solusi, kira-kira solusi atas bug ini seperti apa.)

/label ~bug
